<?php
namespace Website\Models;

use Phalcon\Mvc\Model;

class Blog extends Model
{
    public function initialize()
    {
        $this->setSource("pages");
    }
}