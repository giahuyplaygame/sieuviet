<?php

namespace Website\Models;

use Phalcon\Mvc\Model;
use SVCodebase\Models\ModifyTrait;

class User extends \SVCodebase\Models\BaseModel
{
    public $id;
    public $name;
    public $password;

    use ModifyTrait;

    public function initialize()
    {
        $this->setSource("users");
    }

}