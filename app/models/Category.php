<?php

namespace Website\Models;

use Phalcon\Mvc\Model;
use SVCodebase\Models\ModifyTrait;
//use Website\Models\Category;

class Category extends \SVCodebase\Models\BaseModel
{
    public $id;
    public $name;
    public $parent_id;

    use ModifyTrait;

    public function initialize()
    {
        $this->hasOne('id', Category::class, 'parent_id');
        //$this->setSource("category");
    }

}