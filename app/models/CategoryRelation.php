<?php

namespace Website\Models;

use Phalcon\Mvc\Model;
use SVCodebase\Models\ModifyTrait;

class CategoryRelation extends \SVCodebase\Models\BaseModel
{
    public $id;
    public $name;
    public $parent_id;

    use ModifyTrait;

    public function initialize()
    {
        $this->hasOne('id', 'category', 'parent_id');
        $this->setSource("category");
    }

}