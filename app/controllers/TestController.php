<?php

namespace Website\Controllers;

class TestController extends BaseController
{
    public $name;
    public $age;
    public function onConstruct($name, $age)
    {

    }

    public function showAction()
    {
        echo '<h3 style="color:red">'. $this->name . '</h3>';
        echo '<h3 style="color:red">'. $this->age . '</h3>';
    }

}