<?php

namespace Website\Controllers;

use mysql_xdevapi\Exception;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;
use SVCodebase\Library\UtilArr;
use SVCodebase\Validators\BaseValidate;
use Website\ErrorMessage;
use Website\Models\Category;
use Website\Models\CategoryRelation;
use Website\Models\PlanMonthlyTargetComment;
use Website\Models\User;
use Website\Repos\TestRepo;
use Website\StatusCode;
use Phalcon\Filter;

class IndexController extends BaseController
{
    protected $repo;

    public function onConstruct()
    {
        $this->repo = new TestRepo();
    }

    public function indexAction()
    {

        ('start index controller');
        die('plan service');
    }

    public function error404Action()
    {
        $this->outputJSON(['error' => StatusCode::NOT_FOUND, 'msg' => 'Yêu cầu không hợp lệ.'], StatusCode::NOT_FOUND);
    }

    public function registerAction()
    {
        $rules = [
            'name' => 'required',
            'password' => 'required'
        ];
        $request = (array)$this->request->getJsonRawBody(true);
        BaseValidate::validator($request, $rules);

        $result = $this->repo->createUser();

        return $this->outputSuccess($result);
    }

    public function loginAction()
    {
        $rules = [
            'name' => 'required',
            'password' => 'required'
        ];
        $request = (array)$this->request->getJsonRawBody(true);
        BaseValidate::validator($request, $rules);

        $result = $this->repo->findUser();
        return $this->outputSuccess($result);
    }

    public function fiterAllAction()
    {
        $result = Category::find();

        $filter = new Filter();

        $filter->add(
            'filter',
            function ($value) {
//                var_dump( json_encode($value));die;

                $someArray = json_decode(json_encode($value), true);
                foreach ($someArray as $item) {
                    if ($item['id'] < 5) {
                        $result[] = $item;
                    }
                }
                return $result;
            }
        );
        $filtered = $filter->sanitize($result, 'filter');

        return $this->outputSuccess($filtered);
    }

    public function getAllCateAction()
    {
        $result = Category::find();

        $listTree = $this->showMenuLi3($result->toArray());
        return $this->outputSuccess($listTree);
    }

    public function showMenuLi3($menus)
    {
        foreach ($menus as $key => $item) {
            if ((int)$item['parent_id'] != 0) {
                $menus[$key]['parent'] = $menus[(int)$item['parent_id'] - 1];
            }
        }
        foreach ($menus as $key => $item) {
            if ((int)$item['parent_id'] != 0) {
                $menus[$key]['parent'] = $menus[(int)$item['parent_id'] - 1];
            }
        }
        return $menus;
    }

    public function showMenuLi4()
    {

    }

    public function showMenuLi2($menus)
    {
        foreach ($menus as $key => $item) {
            if ((int)$item['parent_id'] != 0) {
                $item['parent'] = 'test update';
                $menus[$key] = $item;
                $this->showMenuLi2($menus);
            }
        }
        return $menus;
    }

    public function showMenuLi($menus, $step = 0)
    {
        foreach ($menus as $key => $item) {
            if ((int)$item['parent_id'] != 0) {
                $menus[$key]['parent'] = $menus[(int)$item['parent_id'] - 1];
            }
        }
        if ($step < 2) {
            $this->showMenuLi($menus, $step + 1);
        }
        return $menus;
    }

    public function findParent($menus, $parent_id)
    {
        foreach ($menus as $menu) {
            if ($menu['id'] == $parent_id) {
                return $menu;
            }
        }
    }

    public function findCateAction()
    {
        $param = $this->dispatcher->getParam('id');

//        $result = Category::findFirst("id = '" . $param . "'");
//        $result = Category::findFirst(
//            [
//                'id = :id:',
//                'bind' => [
//                    'id' => $param
//                ]
//            ]);
        $result = Category::query()->where('id = :id:', ['id' => $param]);
        echo json_encode($result);
        die;
        $a = $result->getRelated('Category');
        echo json_encode($a);
        die;

        $parent = Category::findFirst("id = '" . $result['parent_id'] . "'");
        if ($parent) {
            $result['parent'] = $parent;
        }

        return $this->outputSuccess($result);
    }

    public function removeCateAction()
    {
        $param = $this->dispatcher->getParam('id');
        $item = Category::findFirst($param);
        if ($item !== false) {
            if ($item->delete() != true) {
                $response['code'] = 400;
                $this->response->setJsonContent($response);
                return $this->response;
//                throw new \InvalidArgumentException('false', '400');

            } else {
                $response['code'] = 200;
                $this->response->setJsonContent($response);
                return $this->response;

//                throw new \InvalidArgumentException('true', '200');
            }
        }
    }


    public function createCateAction()
    {
        $rules = [
            'parent_id' => 'required',
            'name' => 'required'
        ];
        $request = (array)$this->request->getJsonRawBody(true);
        BaseValidate::validator($request, $rules);

        $result = $this->repo->createCate();

        return $this->outputSuccess($result);
    }

    public function diAction()
    {
        $di = $this->getDI();
        $di->set('hihi', function ($name, $age) {
            echo 'name: ' . $name;

            echo 'age: ' . $age;
        });
        $di->get('hihi', ['huy', '20']);

    }

    public function di2Action()
    {
        $di = $this->getDI();
        $di->set('di2', [
            'className' => 'Website\Controllers\TestController',
            'properties' => [
                [
                    'name' => 'name',
                    'value' => [
                        'type' => 'parameter',
                        'value' => 'huy'
                    ]
                ],
                [
                    'name' => 'age',
                    'value' => [
                        'type' => 'parameter',
                        'value' => '25'
                    ]
                ]
            ]
        ]);

        $user = $di->get('di2');
        echo '<pre>';
        print_r($user);
        echo '</pre>';
//        $user->showAction();

    }


    /**
     * @return
     */
    public function testCustomValidateAction()
    {
        $rules = [
            'age' => 'age_than_18',
        ];
        $request = (array)$this->request->getJsonRawBody(true);
        BaseValidate::validator($request, $rules);
        throw new \Exception('Trên 18 tuổi', '200');
    }

    public function testBuilderAdvanceAction()
    {
        $p = Category::buildModel()
            ->groupBy('name')
            ->orderBy('id')
            ->get()->toArray();
        $result = UtilArr::keyBy('name', $p);

        echo json_encode($result);
        die;
        xxx($result);

        echo json_encode($a);
        die;
        foreach ($a as $item) {
            xx($item->name);
        }
        xx($a);
        echo json_encode($a);
        die;

    }

}