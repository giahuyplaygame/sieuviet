<?php

namespace Website\Repos;

use SVCodebase\Library\UtilArr;
use Website\Models\Category;
use Website\Models\PlanMonthlyWorkload;
use Website\Models\Test;
use Website\ErrorMessage;
use Website\Models\User;
use Website\Services\AuthService;
use Website\StatusCode;
use Website\Constant;

class TestRepo extends BaseRepo
{
    protected $authService;

    public function __construct()
    {
        $this->authService = new AuthService();
    }

    public function createOrUpdateWorkloadByMember()
    {
        $request = (array)$this->request->getJsonRawBody(true);

        $id = $request['plan_monthly_workload_id'];

        return $id > 0 ? $this->updateWorkloadByMember($id) : $this->createWorkloadByMember();
    }

    public function createUser()
    {
        $request = (array)$this->request->getJsonRawBody(true);
        $result = new User();
        $result->create($request);
        return $result;
    }

    public function createCate()
    {
        $request = (array)$this->request->getJsonRawBody(true);
        $result = new Category();
        $result->create($request);
        return $result;
    }

    public function findUser()
    {
        $request = (array)$this->request->getJsonRawBody(true);
        $workload = User::buildModel()
            ->wheres('name', '=', $request['name'])
            ->wheres('password', '=', $request['password'])
            ->firstOrFail();
       return $workload;
    }


}